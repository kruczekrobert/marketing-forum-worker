from selenium import webdriver
import keyboard

driver = webdriver.Chrome()

websites = [line.strip() for line in open('websites.txt', 'r')]
usernames = [line.strip() for line in open('usernames.txt', 'r')]
passwords = "Botek123#"
emails = input('Podaj email: ')

website_iter = 0


def url_validate(url_link):
    if url_link.startswith('http'):
        return url_link
    else:
        return "http://{}".format(url_link)


def setup():
    print("W: {}, U: {}".format(len(websites), len(usernames)))  # Check data count
    driver.get(url_validate(websites[website_iter]))  # Open first website


def load_next_website():
    global website_iter
    website_iter += 1
    driver.get(url_validate(websites[website_iter]))  # Open next website


def save_post_url(work=True):
    current_url = driver.current_url
    with open('posts.txt', 'a') as posts_file:
        if work:
            posts_file.write(current_url + '\n')
        else:
            posts_file.write("Wysapil blad" + '\n')

    print("[{}] {}".format(website_iter, current_url))
    load_next_website()


setup()

while True:
    if keyboard.is_pressed('up'):
        keyboard.write(usernames[website_iter])
    if keyboard.is_pressed('left'):
        keyboard.write(passwords)
    if keyboard.is_pressed('down'):
        keyboard.write(emails)
    if keyboard.is_pressed('right'):
        save_post_url()
    if keyboard.is_pressed('home'):
        save_post_url(work=False)

